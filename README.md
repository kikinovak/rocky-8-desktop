# Rocky Linux 8.x KDE Setup

(c) Niki Kovacs, 2025

This repository provides an "automagic" setup script to turn a minimal Rocky
Linux 8 installation into a full-blown enterprise class Linux desktop with
bells and whistles based on KDE. No fancy configuration management here. Just a
bone-headed shell script following the [KISS
principle](https://en.wikipedia.org/wiki/KISS_principle).

## In a nutshell

Perform the following steps:

  - Install a minimal Rocky Linux 8.x system. No graphical environment, no
    GNOME, no applications. 

  - Log in as `root`.

  - Install Git: `dnf install -y git`

  - Grab the script: `git clone https://gitlab.com/kikinovak/rocky-8-desktop`

  - Change into the new directory: `cd rocky-8-desktop`

  - Run the script: `./setup.sh --setup`

  - Grab a cup of coffee while the script does all the work.

  - Switch to graphical mode: `systemctl set-default graphical.target`

  - Reboot.

## Setting up a Rocky Linux KDE desktop

Turning a minimal Rocky Linux installation into a fully functional KDE desktop
boils down to a series of more or less time-consuming operations. Your mileage
may vary of course, but here's what I usually do on a fresh Rocky Linux
installation:

  * Customize the Bash shell : prompt, aliases, locales, etc.

  * Customize the Vim editor.

  * Setup official and third-party repositories.

  * Install a complete set of command line tools.

  * Enable the admin user to access system logs.

  * Configure a persistent password for `sudo`.

  * Install X11 and the basic KDE desktop environment.

  * Remove some unneeded system components.

  * Install a full set of applications.

  * Install Microsoft and Eurostile fonts for better interoperability.

  * Configure the KDE desktop for better usability.

  * Edit application menu entries.

The `setup.sh` script performs all of these operations.

Configure Bash, Vim and SSH:

```
# ./setup.sh --shell
```

Setup official and third-party repositories:

```
# ./setup.sh --repos
```

Install the `Core` and `Base` package groups along with some extra tools:

```
# ./setup.sh --tools
```

Install X11 and the basic KDE desktop environment:

```
# ./setup.sh --graph
```

Configure the KDE desktop environment:

```
# ./setup.sh --kderc
```

Install Microsoft and Eurostile fonts:

```
# ./setup.sh --fonts
```

Install a full set of desktop applications:

```
# ./setup.sh --extra
```

Rewrite most menu entries for better usability:

```
# ./setup.sh --menus
```

Perform all of the above in one go:

```
# ./setup.sh --setup
```

Install DVD/RW-related applications:

```
# ./setup.sh --dvdrw
```

Remove unneeded system components :

```
# ./setup.sh --strip
```

Sync repositories and fetch updates:

```
# ./setup.sh --fresh
```

Apply custom profile for existing users:

```
# ./setup.sh --users
```

Uninstall everything and revert back to minimal installation:

```
# ./setup.sh --reset
```

Display help message:

```
# ./setup.sh --help
```

If you want to know what exactly goes on under the hood, open a second terminal
and view the logs:

```
# tail -f /var/log/setup.log
```

## Discussion

Here's a few remarks, tips and tricks in no particular order.

* Start from a minimal system. No X Window System, no GNOME, no application.
  The script will work if you start it on a GNOME-based Rocky Linux desktop,
  but then you'll have the whole GNOME kitchen sink cluttering your system.

* I've tweaked the default KDE profile to some sane defaults (which you may
  change of course). 

* I've replaced the buggy SDDM login manager with GDM.

* The `--menus` option replaces default menu entries by a series of more
  readable custom entries. I'm providing translations for english, french and
  german for these custom menus. 

&nbsp;

---

*Click on the cup and buy me a coffee.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>
