# Rocky Linux 8.x KDE Roadmap

Feuille de route pour un poste de travail parfait sous Rocky Linux 8.x


## Système de base

* [x] Système de base (Legacy BIOS)

* [x] Système de base (UEFI)

* [x] Système de base (RAID 0)

* [x] Bash personnalisé pour `root`

* [x] Bash personnalisé pour les utilisateurs

* [x] Vim personnalisé

* [x] Ne pas hériter les locales via SSH

* [x] Pas de *timeout* pour `sudo`

* [x] Dépôts de paquets officiels

* [x] Dépôts de paquets EPEL

* [x] Dépôts de paquets ELRepo

* [x] Dépôts de paquets RPMFusion

* [x] Dépôt de paquets Hashicorp

* [x] Dépôt de paquets AnyDesk

* [x] Dépôt de paquets Docker

* [x] Dépôt de paquets Lynis

* [x] Dépôt de paquets EID

* [x] Dépôt de paquets Chrome

* [x] Dépôt de paquets Microlinux

* [x] Dépôt de paquets Flatpak

* [x] Mise à jour du système

* [x] Outils en ligne de commande : `links`, `nmap`, `speedtest`, etc.

* [x] Synchronisation NTP

* [x] Personnalisation de GRUB

* [ ] Résolution de la console

* [x] Désactiver les mitigations CPU


## Serveur X Window

* [x] Serveur graphique X11

* [x] Carte graphique NVidia GT 710

* [x] Carte graphique NVidia GTX 1650


## Polices TrueType

* [x] Jeu de polices TrueType libres

* [x] Polices Microsoft

* [x] Polices Eurostile

* [x] Faire le ménage dans les polices exotiques


## Bureau KDE

* [x] Bureau KDE minimal

* [x] Gestionnaire de connexion GDM

* [x] Supprimer Wayland et User Script

* [x] Option pour ne pas afficher la liste des utilisateurs

* [x] Thème sobre et agréable à l'œil

* [x] Thème unifié Qt/Gtk

* [x] Lanceurs par défaut dans la barre des tâches

* [x] Favoris par défaut dans le menu

* [x] Collection de fonds d'écran

* [x] Entrées de menu personnalisées

* [x] Gestion du réseau : NetworkManager

* [x] Gestion du son

* [ ] Économiseur d'écran style Matrix

* [ ] Associations de fichiers MIME


## Internet

* [x] Navigateur Web : Mozilla Firefox ESR

* [ ] Bloqueur de publicités : uBlock Origin

* [x] Navigateur Web alternatif : Chrome

* [x] Courrier électronique : Mozilla Thunderbird

* [x] Téléchargement BitTorrent : Transmission

* [x] Client IRC : HexChat

* [x] Stockage en ligne : OwnCloud

* [x] Téléchargement FTP : Filezilla

* [ ] Client Citrix : Citrix Workspace App

* [x] Cartes d'identité : eID Viewer

* [ ] Client de connexion Proton VPN

* [x] Messagerie vidéo Skype

* [x] Plateforme de communication Zoom

* [ ] Plateforme de communication Teams


## Bureautique

* [x] Suite bureautique : LibreOffice


## Graphisme

* [x] Visionneur d'images : Gwenview

* [x] Visionneur de documents : Okular

* [x] Gestion de photos : Digikam

* [x] Acquisition d'images : SimpleScan

* [x] Capture d'écran : Spectacle

* [x] Traitement d'images : GIMP

* [x] Images vectorielles : Inkscape

* [x] Logiciel de PAO : Scribus


## Multimédia

* [x] Lecteur audio : Audacious

* [x] Lecteur vidéo : VLC

* [x] Lecture de DVD : DVDCSS

* [x] Lecteur vidéo : MPlayer

* [x] Éditeur audio : Audacity

* [x] Éditeur vidéo : OpenShot

* [x] Logiciel de screencast : OBS Studio

* [x] Normalisation audio : `normalize`

* [x] Extracteur audio : Asunder

* [x] Extracteur vidéo : HandBrake

* [x] Miniatures vidéo : `ffmpegthumbs`


## Système

* [x] Terminal : Konsole

* [x] Surveillance du système : System Monitor

* [x] Moniteur système : Conky

* [x] Informations système : KInfoCenter

* [x] Bureau distant : KRDC

* [x] Imprimantes HP : HPLIP

* [x] Virtualisation : VirtualBox

* [x] Virtualisation : Virtual Machine Manager

* [x] Virtualisation : Vagrant

* [x] Télémaintenance : AnyDesk

* [x] Docker CLI


## Utilitaires

* [x] Éditeur de texte : Kate

* [x] Calculatrice : KCalc

* [x] Gravure de CD/DVD : K3B

* [x] Gestion des mots de passe : KeePassXC

* [x] Outil d'archivage : Ark

* [x] Sélecteur de caractères : KCharSelect

* [x] Smartphone Android : KDE Connect

* [x] Mind Mapping : View Your Mind

* [x] Notes hiérarchisées : CherryTree

* [ ] Outil de recherche : Baloo


## Développement

* [x] Logiciel de TAO : OmegaT

* [ ] Construction RPM : Mock


## Jeux

* [x] Solitaire

* [ ] Steam

