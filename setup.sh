#!/bin/bash
# 
# setup.sh
#
# (c) Niki Kovacs 2025 <info@microlinux.fr>
#
# This script turns a minimal Rocky Linux 8.x installation into a full-blown
# enterprise class Linux desktop with bells and whistles based on KDE.

# Current directory
CWD=$(pwd)

# Default to english for admin tasks
LANG=en_US.UTF-8
export LANG

# Make sure the script is being executed with superuser privileges
if [ "$EUID" -ne 0 ]
then
  echo
  echo "  Please run with sudo or as root." >&2
  echo
  exit 1
fi

# Check distribution and major release number
if [ -f /etc/os-release ]
then
  source /etc/os-release
  SYSTEM="$REDHAT_SUPPORT_PRODUCT"
  VERSION="$(awk -F'[".]' '/^VERSION_ID=/ {print $2}' /etc/os-release)"
fi

# Make sure we're running Rocky Linux 8.x
if [ "$SYSTEM" != "Rocky Linux" ] || [ "$VERSION" != "8" ]
then
  echo
  echo "  Unsupported operating system." >&2
  echo
  exit 1
fi

# Logs
LOG="/var/log/$(basename "$0" .sh).log"

# Slow things down a bit
SLEEP=1

# Admin user
ADMIN=$(getent passwd 1000 | cut -d: -f 1)

# Defined users
USERS="$(awk -F: '$3 > 999 && $3 < 65534 {print $1}' /etc/passwd | sort)"

# Additional command-line tools
TOOLS=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/el8/pkgs/tools.txt)

# Minimal KDE desktop
BASIC=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/el8/pkgs/kdebase.txt)

# Remove these packages
CRUFT=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/el8/pkgs/cruft.txt)

# Additional packages
EXTRA=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/el8/pkgs/extra.txt)

# DVD/RW-related applications
DVDRW="k3b normalize asunder HandBrake-gui"

# Flatpak essentials
FLATPAKS=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/el8/pkgs/flatpaks.txt)

# Custom menus
ENTRIESDIR="$CWD/el8/menu"
ENTRIES=$(ls $ENTRIESDIR)
MENUDIRS="/usr/share/applications \
          /usr/local/share/applications \
          /var/lib/flatpak/exports/share/applications"

# Profile
PLASMA="/usr/share/plasma/plasmoids"
TASKBAR="$PLASMA/org.kde.plasma.taskmanager/contents/config/main.xml"
KICKOFF="$PLASMA/org.kde.plasma.kickoff/contents/config/main.xml"

# Mirrors
DOCKER="https://download.docker.com/linux/rhel"
CHROME="https://dl.google.com/linux"
CISOFY="https://packages.cisofy.com"
ANYDESK="https://keys.anydesk.com/repos"
HASHICORP="https://rpm.releases.hashicorp.com"
RPMFUSION="https://download1.rpmfusion.org"
MICROLINUX="https://pub.microlinux.fr/rockylinux"
EIDBELGIUM="https://eid.belgium.be/sites/default/files/software"

# OwnCloud
OWNCLOUD="https://download.owncloud.com/desktop/ownCloud/stable"
CLOUDVER="5.3.1.14018"

echo
echo "  #########################################"
echo "  # Rocky Linux 8.x desktop configuration #"
echo "  #########################################"
echo

logentry() {

  echo >> $LOG
  echo "+-----------------------------+" >> $LOG
  echo "$(date)" >> $LOG
  echo >> $LOG

}

usage() {

  # Display help message
  echo "  Usage: $0 OPTION"
  echo
  echo "  Options:"
  echo
  echo "    --shell    Configure Bash, Vim, SSH, etc."
  echo "    --repos    Configure official and third-party repositories."
  echo "    --tools    Install full set of command-line tools."
  echo "    --graph    Install graphical environment and basic KDE desktop."
  echo "    --kderc    Configure KDE desktop environment."
  echo "    --fonts    Install Microsoft and Eurostile fonts."
  echo "    --extra    Install full set of desktop applications."
  echo "    --menus    Install custom desktop menu entries."
  echo "    --setup    Perform all of the above in one go."
  echo "    --dvdrw    Install DVD/RW-related applications."
  echo "    --strip    Remove unneeded system components."
  echo "    --fresh    Sync repositories and fetch updates."
  echo "    --users    Apply custom profile for existing users."
  echo "    --reset    Uninstall everything and reset system."
  echo
  echo "  Logs are written to $LOG."
  echo

}

configure_shell() {

  sleep $SLEEP

  echo "  ===== Configure Bash, Vim, SSH, etc. ====="
  echo
  sleep $SLEEP

  echo "  Configuring Bash shell for user: root"
  cp -f $CWD/el8/bash/bashrc-root /root/.bashrc
  sleep $SLEEP

  if [ ! -z "$ADMIN" ]
  then
    if [ -d /home/$ADMIN ]
    then
      echo "  Configuring Bash shell for user: $ADMIN"
      cp -f $CWD/el8/bash/bashrc-user /home/$ADMIN/.bashrc
      chown $ADMIN:$ADMIN /home/$ADMIN/.bashrc
      sleep $SLEEP
    fi
  fi

  echo "  Configuring Bash shell for future users."
  cp -f $CWD/el8/bash/bashrc-user /etc/skel/.bashrc
  sleep $SLEEP

  echo "  Configuring Vim editor for user: root"
  cp -f $CWD/el8/vim/vimrc /root/.vimrc
  sleep $SLEEP

  if [ ! -z "$ADMIN" ]
  then
    if [ -d /home/$ADMIN ]
    then
      echo "  Configuring Vim editor for user: $ADMIN"
      cp -f $CWD/el8/vim/vimrc /home/$ADMIN/.vimrc
      chown $ADMIN:$ADMIN /home/$ADMIN/.vimrc
      sleep $SLEEP
    fi
  fi

  echo "  Configuring Vim editor for future users."
  cp -f $CWD/el8/vim/vimrc /etc/skel/.vimrc
  sleep $SLEEP

  # Don't inherit system locale
  echo "  Configuring SSH server."
  sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
  systemctl reload sshd
  sleep $SLEEP

  # Enable persistent password for sudo
  if [ ! -f /etc/sudoers.d/persistent_password ]
  then
    echo "  Configuring persistent password for sudo."
    logentry
    cp -vf $CWD/el8/sudo/persistent_password /etc/sudoers.d/ >> $LOG
    sleep $SLEEP
  fi

  # Admin user can access system logs
  if [ ! -z "$ADMIN" ]
  then
    if getent group systemd-journal | grep $ADMIN > /dev/null 2>&1
    then
      echo "  Admin user $ADMIN is a member of the systemd-journal group."
    else
      echo "  Adding admin user $ADMIN to systemd-journal group."
      usermod -aG systemd-journal $ADMIN
    fi
    sleep $SLEEP
  fi

  echo

}

configure_repos() {

  sleep $SLEEP

  echo "  ===== Configure official and third-party repositories ====="
  echo
  sleep $SLEEP

  # Priority = 10
  echo "  Configuring official repositories."
  for REPOSITORY in BaseOS AppStream Extras PowerTools Plus Sources
  do
    cp -f $CWD/el8/repo/Rocky-$REPOSITORY.repo /etc/yum.repos.d/
  done
  sleep $SLEEP

  if ! rpm -q epel-release > /dev/null 2>&1
  then
    logentry
    echo "  Installing package: epel-release"
    dnf install -y epel-release >> $LOG 2>&1
  fi

  # Priority = 20
  echo "  Configuring EPEL repositories."
  for FILE in epel epel-modular epel-testing epel-testing-modular
  do
    cp -f $CWD/el8/repo/$FILE.repo /etc/yum.repos.d/
  done
  sleep $SLEEP

  if ! rpm -q elrepo-release > /dev/null 2>&1
  then
    logentry
    echo "  Installing package: elrepo-release"
    dnf install -y elrepo-release >> $LOG 2>&1
  fi

  # Priority = 20
  echo "  Configuring ELRepo repositories."
  cp -f $CWD/el8/repo/elrepo.repo /etc/yum.repos.d/
  sleep $SLEEP

  for RELEASE in free-release free-release-tainted
  do
    if ! rpm -q rpmfusion-$RELEASE > /dev/null 2>&1
    then
      echo "  Installing package: rpmfusion-$RELEASE"
      logentry
      dnf install -y rpmfusion-$RELEASE >> $LOG 2>&1
    fi
  done

  if ! rpm -q rpmfusion-nonfree-release > /dev/null 2>&1
  then
    echo "  Installing package: rpmfusion-nonfree-release"
    logentry
    dnf install -y \
      $RPMFUSION/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm \
      >> $LOG 2>&1
  fi

  # Priority = 20
  echo "  Configuring RPM Fusion repositories."
  for REPOFILE in free-tainted \
                  free-updates \
                  free-updates-testing \
                  nonfree-updates \
                  nonfree-updates-testing
  do
    cp -f $CWD/el8/repo/rpmfusion-$REPOFILE.repo /etc/yum.repos.d/
  done
  sleep $SLEEP

  # Priority = 10
  echo "  Configuring Lynis repository."
  rpm --import $CISOFY/keys/cisofy-software-rpms-public.key
  cp -f $CWD/el8/repo/lynis.repo /etc/yum.repos.d/
  sleep $SLEEP

  # Priority = 20
  echo "  Configuring HashiCorp repository."
  rpm --import $HASHICORP/gpg >> $LOG 2>&1
  cp -f $CWD/el8/repo/hashicorp.repo /etc/yum.repos.d/
  sleep $SLEEP

  # Priority = 20
  echo "  Configuring Docker repository."
  rpm --import $DOCKER/gpg
  cp -f $CWD/el8/repo/docker.repo /etc/yum.repos.d/
  sleep $SLEEP

  # Priority = 20
  echo "  Configuring AnyDesk repository."
  rpm --import $ANYDESK/RPM-GPG-KEY
  cp -f $CWD/el8/repo/anydesk.repo /etc/yum.repos.d/
  sleep $SLEEP

  # Priority = 20
  echo "  Configuring Chrome repository."
  rpm --import $CHROME/linux_signing_key.pub
  cp -f $CWD/el8/repo/google-chrome.repo /etc/yum.repos.d/
  sleep $SLEEP

  # Priority = 20
  if ! rpm -q eid-archive-el > /dev/null 2>&1
  then
     echo "  Installing package: eid-archive-el"
     logentry
     rpm -ivh $EIDBELGIUM/eid-archive-el-2021-1.noarch.rpm >> $LOG 2>&1
     sleep $SLEEP
  fi
  echo "  Configuring EID Archive repository."
  cp -f $CWD/el8/repo/eid-archive.repo /etc/yum.repos.d/
  sleep $SLEEP

  # Priority = 20
  echo "  Configuring Microlinux repository."
  rpm --import $MICROLINUX/RPM-GPG-KEY >> $LOG 2>&1
  cp -f $CWD/el8/repo/microlinux.repo /etc/yum.repos.d/
  sleep $SLEEP

  # Don't install weak dependencies
  if ! grep install_weak_deps /etc/dnf/dnf.conf > /dev/null
  then
    echo "  Disabling weak dependency installation."
    echo "install_weak_deps=False" >> /etc/dnf/dnf.conf
  fi

  echo

}

install_tools() {

  sleep $SLEEP

  echo "  ===== Install full set of command-line tools ====="
  echo
  sleep $SLEEP

  echo "  Fetching missing packages from Core package group." 
  logentry
  dnf group install -y "Core" >> $LOG 2>&1
  echo "  Core package group is installed."
  sleep $SLEEP

  echo "  Installing Base package group."
  sleep $SLEEP
  echo "  This might take a moment."
  echo "  Logs are written to $LOG."
  dnf group install -y "Base" >> $LOG 2>&1
  echo "  Base package group is installed."
  sleep $SLEEP

  echo "  Installing some additional tools."
  sleep $SLEEP
  for PACKAGE in $TOOLS
  do
    if ! rpm -q $PACKAGE > /dev/null 2>&1
    then
      echo "  Installing package: $PACKAGE"
      logentry
      dnf install -y $PACKAGE >> $LOG 2>&1
    fi
  done

  echo "  Full set of command-line tools installed."
  sleep $SLEEP

  echo

}

install_kde() {

  sleep $SLEEP

  echo "  ===== Install graphical environment and basic KDE desktop ====="
  echo
  sleep $SLEEP

  echo "  Installing X Window System."
  sleep $SLEEP
  echo "  This might take a moment."
  sleep $SLEEP
  echo "  Logs are written to $LOG."
  logentry
  dnf group install -y "base-x" >> $LOG 2>&1
  dnf install -y xorg-x11-fonts-* >> $LOG 2>&1
  dnf install -y nvidia-detect >> $LOG 2>&1
  echo "  X Window System is installed."
  sleep $SLEEP

  echo "  Installing basic KDE desktop."
  sleep $SLEEP
  for PACKAGE in $BASIC
  do
    if ! rpm -q $PACKAGE > /dev/null 2>&1
    then
      echo "  Installing package: $PACKAGE"
      dnf install -y $PACKAGE >> $LOG 2>&1
    fi
  done

  if ! grep -q flathub /var/lib/flatpak/repo/config > /dev/null 2>&1
  then
    echo "  Configuring Flathub repository."
    flatpak remote-add --if-not-exists \
      flathub https://flathub.org/repo/flathub.flatpakrepo >> $LOG 
    sleep $SLEEP
  fi

  echo "  Basic KDE desktop is installed."
  sleep $SLEEP

  echo

}

configure_kde() {

  sleep $SLEEP

  echo "  ===== Configure KDE desktop environment ====="
  echo
  sleep $SLEEP

  echo "  Default to Plasma X11 session."
  cp -f $CWD/el8/gdm/administrator /etc/accountsservice/user-templates/
  cp -f $CWD/el8/gdm/standard /etc/accountsservice/user-templates/
  sleep $SLEEP

  # Disable all Wayland sessions
  for FILE in $(ls /usr/share/wayland-sessions)
  do
    if ! grep NoDisplay=true /usr/share/wayland-sessions/$FILE > /dev/null
    then
      echo "  Disabling Wayland session for: $FILE"
      echo "NoDisplay=true" >> /usr/share/wayland-sessions/$FILE
      sleep $SLEEP
    fi
  done

  # Disable all X11 sessions except Plasma
  for FILE in $(ls /usr/share/xsessions --ignore=plasma.desktop)
  do
    if ! grep NoDisplay=true /usr/share/xsessions/$FILE > /dev/null
    then
      echo "  Disabling X11 session for: $FILE"
      echo "NoDisplay=true" >> /usr/share/xsessions/$FILE
      sleep $SLEEP
    fi
  done

  echo "  Installing custom logo for GDM."
  cp -f $CWD/el8/logo/microlinux-logo.png \
    /usr/share/pixmaps/fedora-gdm-logo.png
  sleep $SLEEP

  echo "  Configuring taskbar."
  sed -i -e 's/applications:systemsettings.desktop/applications:org.kde.dolphin.desktop/g' $TASKBAR
  sed -i -e 's/applications:org.kde.discover.desktop/applications:firefox.desktop/g' $TASKBAR
  sed -i -e 's/preferred:\/\/filemanager/applications:thunderbird.desktop/g' $TASKBAR
  sed -i -e 's/preferred:\/\/browser/applications:libreoffice-startcenter.desktop/g' $TASKBAR
  sleep $SLEEP

  echo "  Configuring Kickoff menu."
  sed -i -e 's/preferred:\/\/browser,//g' $KICKOFF
  sed -i -e 's/org.kde.kontact.desktop,//g' $KICKOFF
  sed -i -e 's/kontact.desktop,//g' $KICKOFF
  sed -i -e 's/systemsettings.desktop,//g' $KICKOFF
  sed -i -e 's/org.kde.ktpcontactlist.desktop,ktpcontactlist.desktop/applications:firefox.desktop/g' $KICKOFF
  sed -i -e 's/org.kde.kate.desktop,kate.desktop/applications:thunderbird.desktop/g' $KICKOFF
  sed -i -e 's/org.kde.konsole.desktop/applications:libreoffice-startcenter.desktop/g' $KICKOFF
  sed -i -e 's/org.kde.apper.desktop,//g' $KICKOFF
  sed -i -e 's/apper.desktop,//g' $KICKOFF
  sed -i -e 's/org.kde.discover.desktop//g' $KICKOFF
  sed -i -e 's/suspend,hibernate/logout/g' $KICKOFF
  sleep $SLEEP

  echo "  Replace ugly default wallpaper."
  cp -f $CWD/el8/wall/default-wallpaper-01-day.png \
    /usr/share/backgrounds/f36/default/f36-01-day.png
  cp -f $CWD/el8/wall/default-wallpaper-02-night.png \
    /usr/share/backgrounds/f36/default/f36-02-night.png
  sleep $SLEEP

  echo "  Use default wallpaper for lock screen."
  rm -f /usr/share/backgrounds/default.png
  ln -s /usr/share/backgrounds/f36/default/f36-01-day.png \
    /usr/share/backgrounds/default.png
  sleep $SLEEP

  if [ ! -d /etc/skel/.config ]
  then
    echo "  Creating configuration directory."
    mkdir /etc/skel/.config
    sleep $SLEEP
  fi

  echo "  Deactivating desktop search for future users."
  cp -f $CWD/el8/kde/baloofilerc /etc/skel/.config/
  sleep $SLEEP

  echo "  Configuring lock screen for future users."
  cp -f $CWD/el8/kde/kscreenlockerrc /etc/skel/.config/
  sleep $SLEEP

  echo "  Configuring default desktop theme for future users."
  cp -f $CWD/el8/kde/plasmarc /etc/skel/.config/
  sleep $SLEEP
  
  echo "  Configuring desktop effects for future users."
  cp -f $CWD/el8/kde/kwinrc /etc/skel/.config/
  sleep $SLEEP

  if [ ! -z "$ADMIN" ]
  then
    if [ -d /home/$ADMIN ]
      then
        if [ ! -d /home/$ADMIN/.config ]
        then
          echo "  Creating configuration directory for user: $ADMIN"
          mkdir /home/$ADMIN/.config
          chown $ADMIN:$ADMIN /home/$ADMIN/.config
          sleep $SLEEP
        fi
        echo "  Deactivating desktop search for user: $ADMIN"
        cp -f $CWD/el8/kde/baloofilerc /home/$ADMIN/.config/
        chown $ADMIN:$ADMIN /home/$ADMIN/.config/baloofilerc
        sleep $SLEEP
        echo "  Configuring lock screen for user: $ADMIN"
        cp -f $CWD/el8/kde/kscreenlockerrc /home/$ADMIN/.config/
        chown $ADMIN:$ADMIN /home/$ADMIN/.config/kscreenlockerrc
        sleep $SLEEP
        echo "  Configuring default desktop theme for user: $ADMIN"
        cp -f $CWD/el8/kde/plasmarc /home/$ADMIN/.config/
        chown $ADMIN:$ADMIN /home/$ADMIN/.config/plasmarc
        sleep $SLEEP
        echo "  Configuring desktop effects for user: $ADMIN"
        cp -f $CWD/el8/kde/kwinrc /home/$ADMIN/.config/
        chown $ADMIN:$ADMIN /home/$ADMIN/.config/kwinrc
        sleep $SLEEP
    fi
  fi

  echo "  KDE desktop environment configured."
  sleep $SLEEP

  echo

}

install_fonts() {

  echo "  ===== Install Microsoft and Eurostile fonts ====="
  echo
  sleep $SLEEP

  # Microsoft TrueType fonts
  if [ ! -d /usr/share/fonts/microsoft ]
  then
    pushd /tmp > /dev/null
    rm -rf /usr/share/fonts/microsoft webcore-fonts symbol
    echo "  Installing Microsoft TrueType fonts."
    logentry
    wget -c --no-check-certificate \
      https://ponce.cc/slackware/sources/repo/webcore-fonts-3.0.tar.gz \
      >> $LOG 2>&1
    logentry
    wget -c --no-check-certificate \
      https://ponce.cc/slackware/sources/repo/symbol.gz >> $LOG 2>&1
    mkdir /usr/share/fonts/microsoft
    tar xzf webcore-fonts-3.0.tar.gz
    gunzip symbol.gz
    pushd webcore-fonts > /dev/null
    if type fontforge > /dev/null
    then
      logentry
      fontforge -lang=ff -c 'Open("vista/CAMBRIA.TTC(Cambria)"); \
        Generate("vista/CAMBRIA.TTF");Close();Open("vista/CAMBRIA.TTC(Cambria Math)"); \
        Generate("vista/CAMBRIA-MATH.TTF");Close();' >> $LOG 2>&1
      rm vista/CAMBRIA.TTC
    fi
    cp -v fonts/* /usr/share/fonts/microsoft/ >> $LOG
    cp -v vista/* /usr/share/fonts/microsoft/ >> $LOG
    popd > /dev/null
    # Substitute the symbol.ttf font with a patched version
    # https://bugs.winehq.org/show_bug.cgi?id=24099
    cp -vf symbol /usr/share/fonts/microsoft/symbol.ttf >> $LOG
    fc-cache -f
    popd > /dev/null
  fi

  # Eurostile fonts
  if [ ! -d /usr/share/fonts/eurostile ]
  then
    pushd /tmp > /dev/null
    rm -rf /usr/share/fonts/eurostile
    echo "  Installing Eurostile TrueType fonts."
    wget -c --no-check-certificate \
      https://www.microlinux.fr/download/Eurostile.zip >> $LOG 2>&1
    unzip Eurostile.zip -d /usr/share/fonts/ >> $LOG
    mv -v /usr/share/fonts/Eurostile /usr/share/fonts/eurostile >> $LOG
    fc-cache -f
    rm -f Eurostile.zip
    popd > /dev/null
  fi

  echo "  Microsoft and Eurostile fonts installed on the system."
  sleep $SLEEP

  echo

}

install_apps() {

  sleep $SLEEP

  echo "  ===== Install full set of desktop applications ====="
  echo
  sleep $SLEEP

  for PACKAGE in $EXTRA
  do
    if ! rpm -q $PACKAGE > /dev/null 2>&1
    then
      echo "  Installing package: $PACKAGE"
      logentry
      dnf install -y $PACKAGE >> $LOG 2>&1
    fi
  done

  for FLATPAK in $FLATPAKS
  do
    if ! flatpak info $FLATPAK > /dev/null 2>&1
    then
      echo "  Installing Flatpak: $FLATPAK"
      logentry
      flatpak install -y $FLATPAK >> $LOG 2>&1
    fi
  done

  if ! rpm -q owncloud-client > /dev/null 2>&1
  then
    echo "  Installing AppImage: owncloud-client"
    logentry
    dnf install -y \
      $OWNCLOUD/$CLOUDVER/linux-appimage/owncloud-client_${CLOUDVER}.rpm \
      >> $LOG
  fi

  echo "  Full set of desktop applications installed."
  sleep $SLEEP

  echo

}

rewrite_menus() {

  echo "  ===== Install custom desktop menu entries ====="
  echo
  sleep $SLEEP

  for MENUDIR in $MENUDIRS
  do
    for ENTRY in $ENTRIES
    do
      if [ -r $MENUDIR/$ENTRY ]
      then
        echo "  Rewriting menu item: $ENTRY"
        sed -i '/^#/d' $MENUDIR/$ENTRY
        sed -i '/^\[Desktop Action/Q' $MENUDIR/$ENTRY
        sed -i '/^\[X-Drawing/Q' $MENUDIR/$ENTRY
        sed -i '/^\[X-Property/Q' $MENUDIR/$ENTRY
        sed -i '/^GenericName/d' $MENUDIR/$ENTRY
        sed -i '/^Name/d' $MENUDIR/$ENTRY
        sed -i '/^Comment/d' $MENUDIR/$ENTRY
        sed -i '/^Keywords/d' $MENUDIR/$ENTRY
        sed -i '/^Actions/d' $MENUDIR/$ENTRY
        sed -i '/^Version/d' $MENUDIR/$ENTRY
        sed -i '/^Categories/d' $MENUDIR/$ENTRY
        sed -i '/^NoDisplay/d' $MENUDIR/$ENTRY
        sed -i '/^$/d' $MENUDIR/$ENTRY
        cat $ENTRIESDIR/$ENTRY >> $MENUDIR/$ENTRY
        sleep 0.1
      fi
    done
  done

  # OpenJDK menu entry is a moving target
  if rpm -q java-1.8.0-openjdk > /dev/null 2>&1
  then 
    JDKMENU=$(rpm -ql java-1.8.0-openjdk | grep desktop)
    if ! grep -q NoDisplay $JDKMENU
    then
      echo "  Hiding OpenJDK menu entry."
      echo "NoDisplay=true" >> $JDKMENU
      sleep $SLEEP
    fi
  fi

  echo "  Updating desktop database."
  update-desktop-database
  sleep $SLEEP

  echo "  Custom desktop menu installed."
  sleep $SLEEP

  echo

}

install_dvdrw() {

  sleep $SLEEP

  echo "  ===== Install DVD/RW-related applications ====="
  echo
  sleep $SLEEP

  for PACKAGE in $DVDRW

  do
    if ! rpm -q $PACKAGE > /dev/null 2>&1
    then
      echo "  Installing package: $PACKAGE"
      logentry
      dnf install -y $PACKAGE >> $LOG 2>&1
    fi
  done

  echo "  DVD/RW-related applications installed."
  sleep $SLEEP

  echo

}

remove_cruft() {

  sleep $SLEEP

  echo "  ===== Remove unneeded system components ====="
  echo
  sleep $SLEEP

  for PACKAGE in $CRUFT
  do
    if rpm -q $PACKAGE > /dev/null 2>&1
    then
      logentry
      echo "  Removing package: $PACKAGE"
      dnf remove -y $PACKAGE >> $LOG 2>&1
    fi
  done

  echo "  Unneeded components removed from the system."
  sleep $SLEEP

  echo

}

update_system() {

  sleep $SLEEP

  echo "  ===== Sync repositories and fetch updates ====="
  echo
  sleep $SLEEP

  echo "  Cleaning package cache."
  logentry
  dnf clean all >> $LOG
  sleep $SLEEP

  echo "  Downloading metadata for enabled repositories."
  dnf makecache >> $LOG

  echo "  Performing system update."
  sleep $SLEEP
  echo "  This might take a moment."
  sleep $SLEEP
  echo "  Logs are written to $LOG."
  dnf -y update >> $LOG
  flatpak -y update >> $LOG

  echo "  System is up to date."
  sleep $SLEEP

  echo

}

apply_profile() {

  echo "  ===== Apply custom profile ====="
  echo
  sleep $SLEEP

  if [ ! -z "$USERS" ]
  then
    for USER in $USERS
    do
      if [ -d /home/$USER ]
      then
        echo "  Configuring Bash shell for user: $USER"
        cp -f $CWD/el8/bash/bashrc-user /home/$USER/.bashrc
        chown $USER:$USER /home/$ADMIN/.bashrc
        sleep $SLEEP
        echo "  Configuring Vim editor for user: $USER"
        cp -f $CWD/el8/vim/vimrc /home/$USER/.vimrc
        chown $USER:$USER /home/$USER/.vimrc
        sleep $SLEEP
        if [ ! -d /home/$USER/.config ]
        then
          echo "  Creating configuration directory for user: $USER"
          mkdir /home/$USER/.config
          chown $USER:$USER /home/$USER/.config
          sleep $SLEEP
        fi
        echo "  Deactivating desktop search for user: $USER"
        cp -f $CWD/el8/kde/baloofilerc /home/$USER/.config/
        chown $USER:$USER /home/$USER/.config/baloofilerc
        sleep $SLEEP
        echo "  Configuring lock screen for user: $USER"
        cp -f $CWD/el8/kde/kscreenlockerrc /home/$USER/.config/
        chown $USER:$USER /home/$USER/.config/kscreenlockerrc
        sleep $SLEEP
        echo "  Configuring default desktop theme for user: $USER"
        cp -f $CWD/el8/kde/plasmarc /home/$USER/.config/
        chown $USER:$USER /home/$USER/.config/plasmarc
        sleep $SLEEP
        echo "  Configuring desktop effects for user: $USER"
        cp -f $CWD/el8/kde/kwinrc /home/$USER/.config/
        chown $USER:$USER /home/$USER/.config/kwinrc
        sleep $SLEEP
      fi
    done
  fi

  echo "  Custom profiles installed for all users."
  sleep $SLEEP

  echo

}

reset_system() {

  echo "  ===== Reset system ====="
  echo
  sleep $SLEEP

  if [ "$(systemctl is-active graphical.target)" == "active" ]
  then
    echo "  This option can only be used in multi-user.target mode."
    echo
    sleep $SLEEP
    exit 1
  fi

  if [ -x /usr/bin/flatpak ]
  then

    echo "  Removing all Flatpaks."
    flatpak uninstall -y --all > /dev/null 2>&1
    sleep $SLEEP

    if flatpak remotes | grep flathub > /dev/null 2>&1
    then
      echo "  Removing Flathub repository configuration."
      flatpak remote-delete flathub
      sleep $SLEEP
    fi

  fi

  for PACKAGE in $CRUFT $DVDRW $EXTRA $BASIC $TOOLS
  do
    if rpm -q $PACKAGE > /dev/null 2>&1
    then
      logentry
      echo "  Removing package: $PACKAGE"
      dnf remove -y $PACKAGE >> $LOG 2>&1
    fi
  done

  echo "  Removing extra packages."
  dnf remove -y nvidia* owncloud* kde* kf5* > /dev/null 2>&1
  sleep $SLEEP

  echo "  Removing X Window System."
  dnf group remove -y base-x > /dev/null 2>&1
  dnf remove -y xorg-x11-fonts-* > /dev/null 2>&1
  dnf remove -y dejavu*fonts* > /dev/null 2>&1
  dnf remove -y google*fonts* > /dev/null 2>&1
  sleep $SLEEP

  echo "  Removing third-party repositories."
  dnf remove -y eid-archive-el > /dev/null 2>&1
  dnf remove -y rpmfusion* > /dev/null 2>&1
  dnf remove -y elrepo-release > /dev/null 2>&1
  dnf remove -y epel-release > /dev/null 2>&1
  rm -f /etc/yum.repos.d/*.repo.rpmsave
  for REPOSITORY in anydesk docker google-chrome hashicorp lynis microlinux
  do 
    rm -f /etc/yum.repos.d/$REPOSITORY.repo
  done
  sleep $SLEEP

  echo "  System reset complete."
  sleep $SLEEP

  echo

}


# Check parameters.
if [[ "$#" -ne 1 ]]
then
  usage
  exit 1
fi
OPTION="$1"
case "$OPTION" in
  --shell)
    configure_shell
    ;;
  --repos)
    configure_repos
    ;;
  --tools)
    install_tools
    ;;
  --graph)
    install_kde
    ;;
  --kderc)
    configure_kde
    ;;
  --fonts)
    install_fonts
    ;;
  --extra)
    install_apps
    ;;
  --menus)
    rewrite_menus
    ;;
  --setup)
    configure_shell
    configure_repos
    install_tools
    install_kde
    configure_kde
    install_fonts
    install_apps
    rewrite_menus
    ;;
  --dvdrw)
    install_dvdrw
    ;;
  --strip)
    remove_cruft
    ;;
  --fresh)
    update_system
    ;;
  --users)
    apply_profile
    ;;
  --reset)
    reset_system
    ;;
  --help)
    usage
    exit 0
    ;;
  ?*)
    usage
    exit 1
esac

exit 0
